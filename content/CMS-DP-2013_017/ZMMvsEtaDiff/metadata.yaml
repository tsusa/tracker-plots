title: '$Z^{0}$ peak mass as a function of the pseudorapidity difference $\Delta \eta$'
caption: 'Distortions of the tracker geometry can lead to a bias in the reconstructed track curvature $\kappa \propto \pm 1/p_{T}$. 

These are investigated using the reconstructed $Z\rightarrow\mu\mu$ mass, as a function of the muon direction and separating $\mu^{+}$ and $\mu^{-}$ (since curvature bias has opposite effect on their $p_{T}$).
Invariant mass distribution is fitted to a Breit-Wigner convoluted with a Crystal ball function (i.e. taking into account the finite track resolution and the radiative tail) for the signal plus an exponential background. Fit range is 75-105 GeV/$c^{2}$, $Z^{0}$ width fixed to PDG value of 2.495 GeV/$c^{2}$.
This does not show the CMS muon reconstruction and calibration performance! It is the tracker input to all object reconstruction. Physics analyses apply momentum calibration on top of this. 

The plots show the $Z^{0}$ peak mass as a function of the pseudorapidity difference $\Delta \eta = \eta(\mu^{+}) - \eta(\mu^{-})$. Almost perfectly constant as a function of $\eta(\mu^{+}) - \eta(\mu^{-})$. A twist distortion ($\Delta \phi \propto z$) would introduce a slope. Fixed due to use of $Z\rightarrow\mu^{+}\mu^{-}$ mass in the alignment fit.
'
date: '2013-05-15'
tags:
- Run-1
- pp
- Collisions
- 2011
- 2012
- Tracker
- Alignment