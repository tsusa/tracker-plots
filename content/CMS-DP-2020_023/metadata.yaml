title: 'CMS Tracker Alignment Parameter Errors performance results for full Run 2 Legacy reprocessing'
abstract: 'During the LHC Long Shutdown 2, the CMS experiment started a collaboration-wide effort to recalibrate the data and optimize the simulation, reconstruction and analysis software in order to reprocess the data collected during LHC Run 2 with uniform software and detector calibrations (Legacy
processing). This note collects performance results of the tracker detector alignment parameter errors obtained using the updated Legacy software and calibration for the data collected by CMS during the three years period 2016,2017 and 2018, and corresponding to an integrated luminosity of about 140/fb. The results include the comparison of the performance of the updated conditions to the one of the alignment and calibration used during data-taking, as well as the one derived at the end of each data-taking year. The derivation of the alignment position errors (APEs) is based on the following procedure: 
$\bullet$ The normalized hit residual distributions $(x_{trk} - x_{hit})/\sigma$, where $\sigma$ is the quadratic sum of the cluster position estimation (CPE) error and the track uncertainty, should have the same width as design MC simulation. 
$\bullet$ Misalignment broadens the hit residual distribution
$\bullet$ Introduce additional uncertainty such that the total residual uncertainty is given by: $\sigma_{r}^2 = \sigma^2 + \sigma_{align}^2$
$\bullet$ if $\sigma_{align}$ correctly estimated: $(x_{trk} - x_{hit})/\sigma_r$ has a width of unity, assuming the CPE error is estimated correctly.
The APEs are needed for $\chi^2$/ndf determination of track candidates, included in the tracking covariance matrix and crucial for the hit association windows used in pattern recognition.'

date: '2020-04-16'
CDS: https://cds.cern.ch/record/2717927
iCMS: https://cms.cern.ch/iCMS/user/noteinfo?cmsnoteid=CMS%20DP-2020/023