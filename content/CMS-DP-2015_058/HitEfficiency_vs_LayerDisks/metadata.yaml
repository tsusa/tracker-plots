title: 'Hit Efficiency of Pixel Barrel Layers and Forward Disks.'
caption: 'Hit efficiency measurement 

  $\bullet$ Hit efficiency is the probability to find any clusters within a 500 $\mu$m area around an expected hit:
 
   – The goal is to quantify pure sensor efficiency with tracking effects maximally removed; 

   – Temporary efficiency loss is observed in function of hit-rate (dynamic inefficiency most dominantly due to buffer overflow); largest effect is visible in the innermost layer (Layer 1).    
 
  $\bullet$  Expected hits are provided by good quality tracks: 

   – Associated to primary vertex with small impact parameter; 

   – With p$_{T}$ > 1.0 GeV; 

   – Missing hit is allowed only on layer under investigation (valid hits are expected on two „other” layers/disks).

 $\bullet$  Module selection: 
   – Bad read-outs removed;
 
   – Read-out chips (ROC) under SEU (temporarily unfunctional ROCs) removed;
 
   – ROC and module edges, as well as, overlap areas of adjacent modules within a layer rejected; 

   – Only modules with good illumination by tracks are selected; 

 $\bullet$  Error bars are dominated by systematic uncertainty estimated to be ~0.3%. 

The plot shows average layer efficiency over all runs in 2015 with 25 ns bunch structure: 

  $\bullet$ The statistical error in the measurement is negligible, systematic uncertainty of 0.3% is shown as error bars;  

  $\bullet$ Significant efficiency loss is observed in Layer 1; 

  $\bullet$ Average loss in 2015 is comparable (slightly less) to 2012 results (see e.g. Fig. 5 in 2014 JINST 9 C03005)' 

date: '2015/12/15'
tags:
- Run-2
- Phase-0 Pixel
- 2015
- pp  
- Collisions
